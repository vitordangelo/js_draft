const arrayObj = [{ "line": "Varginha - PA", "price": 15.3 }, { "line": "Varginha - PA", "price": 20.1 }]
const reducer = (accumulator, currentValue) => accumulator + currentValue;

const arrayPrice = arrayObj.map(obj => obj.price)
console.log(arrayPrice.reduce(reducer))